/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package javanetclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    private static final String SERVER_ADDRESS = "192.168.1.99";
    private static final int SERVER_PORT = 4598;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean working = true;

        while (working) {
            System.out.println("+-------------------------+");
            System.out.printf("| %-23s |\n", "MENU");
            System.out.println("+=========================+");
            System.out.printf("| %d. %20s |\n", 1, "[S]end message");
            System.out.println("+-------------------------+");
            System.out.printf("| %d. %20s |\n", 0, "[E]xit");
            System.out.println("+-------------------------+");

            String input = scanner.nextLine();

            switch (input.toLowerCase()) {
                case "1":
                case "s":
                    System.out.println("Input address: ");
                    String address = scanner.nextLine();
                    System.out.println("Input message: ");
                    String message = scanner.nextLine();
                    
                    sendMessage(address, message);
                    break;
                case "0":
                case "e":
                    working = false;
                    continue;
            }

            break;
        }

    }

    public static void sendMessage(String address, String message) {
        InetSocketAddress isa = new InetSocketAddress(SERVER_ADDRESS, SERVER_PORT);

        try (Socket client = new Socket()) {
            System.out.println("CONNECTING...");
            client.connect(isa);
            System.out.println("CONNECTION ESTABLISHED");

            DataOutputStream dos = new DataOutputStream(client.getOutputStream());
            DataInputStream dis = new DataInputStream(client.getInputStream());

            dos.writeUTF("PING");
            dos.flush();

            String response = dis.readUTF();

            System.out.println(response);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("CONNECTION CLOSE");
        }
    }

}
