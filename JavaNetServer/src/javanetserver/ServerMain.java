/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package javanetserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class ServerMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try (ServerSocket serverSocket = new ServerSocket(6776)) {
            while (true) {
                // Wait for client.
                try (Socket accept = serverSocket.accept()) {
                    accept.setSoLinger(true, 1000);
                    // Accept client and with him.
                    // Get client address info
                    InetAddress inetAddress = accept.getInetAddress();
                    // Get IP address
                    String clientIP = inetAddress.getHostName();

                    System.out.println(clientIP);

                    DataInputStream dis = new DataInputStream(accept.getInputStream());
                    DataOutputStream dos = new DataOutputStream(accept.getOutputStream());

                    String command = dis.readUTF();

                    switch (command) {
                        case "PING":
                            dos.writeUTF("PONG");
                            dos.flush();
                            break;
                        default:
                            dos.writeUTF(String.format("ERROR: Commnd '%s' is not support!", command));
                            dos.flush();
                            break;
                    }
//                    accept.close(); 
                } catch (IOException ex) {
                    Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
